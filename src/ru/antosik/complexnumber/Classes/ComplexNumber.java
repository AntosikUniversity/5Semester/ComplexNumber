package ru.antosik.complexnumber.Classes;

/**
 * Class, which implements Complex Number
 */
public class ComplexNumber {
    private final double real;
    private final double imagine;

    /**
     * Constructor without args
     */
    public ComplexNumber() {
        real = 0;
        imagine = 0;
    }

    /**
     * Constructor with real part
     *
     * @param re Real part of complex number
     */
    public ComplexNumber(double re) {
        real = re;
        imagine = 0;
    }

    /**
     * Constructor with real & imagine parts
     *
     * @param re Real part of complex number
     * @param im Imagine part of complex number
     */
    public ComplexNumber(double re, double im) {
        real = re;
        imagine = im;
    }

    /**
     * Constructor which copies another number
     *
     * @param number Another complex number
     */
    public ComplexNumber(ComplexNumber number) {
        real = number.real;
        imagine = number.imagine;
    }

    /**
     * Overrided ToString method
     *
     * @return String presentation of Complex number
     */
    @Override
    public String toString() {
        if (real == 0 && imagine == 0) return "0";
        if (real == 0) return String.valueOf(imagine) + 'i';

        StringBuilder sb = new StringBuilder(String.valueOf(real));
        if (imagine > 0) sb.append("+").append(imagine).append('i');
        else if (imagine < 0) sb.append(imagine).append('i');
        return sb.toString();
    }

    /**
     * Negates current number
     *
     * @return Negated number
     */
    public ComplexNumber negate() {
        return new ComplexNumber(
                -1 * real,
                -1 * imagine
        );
    }

    /**
     * Perform "+" operator
     *
     * @param second Second complex number
     * @return Sum of numbers
     */
    public ComplexNumber add(ComplexNumber second) {
        return new ComplexNumber(
                real + second.real,
                imagine + second.imagine
        );
    }

    /**
     * Perform "-" operator
     *
     * @param second Second complex number
     * @return Subtraction of numbers
     */
    public ComplexNumber substract(ComplexNumber second) {
        return new ComplexNumber(
                real - second.real,
                imagine - second.imagine
        );
    }

    /**
     * Perform "*" operator
     *
     * @param second Second complex number
     * @return Multiplication of numbers
     */
    public ComplexNumber multiply(ComplexNumber second) {
        double newReal = real * second.real - imagine * second.imagine;
        double newImagine = imagine * second.real + real * second.imagine;
        return new ComplexNumber(
                newReal,
                newImagine
        );
    }

    /**
     * Perform "/" operator
     *
     * @param second Second complex number
     * @return Divide of numbers
     */
    public ComplexNumber divide(ComplexNumber second) {
        if (second.imagine == 0 && second.real == 0) throw new ArithmeticException("Divide by zero!");
        double newReal = (real * second.real + imagine * second.imagine) / (second.real * second.real + second.imagine * second.imagine);
        double newImagine = (imagine * second.real - real * second.imagine) / (second.real * second.real + second.imagine * second.imagine);
        return new ComplexNumber(
                newReal,
                newImagine
        );
    }

    /**
     * Perform module operation
     *
     * @return Module of complex number
     */
    public double module() {
        return Math.sqrt(real * real + imagine * imagine);
    }

    /**
     * Perform "^" operator
     *
     * @param num Pow
     * @return Complex number in pow
     */
    public ComplexNumber pow(double num) {
        double module = module();
        double varphi = (real != 0) ? Math.acos(real / module) : Math.asin(imagine / module);
        return new ComplexNumber(
                Math.pow(module, num) * Math.cos(num * varphi),
                Math.pow(module, num) * Math.sin(num * varphi)
        );
    }

    /**
     * Perform root operator
     *
     * @param num Root
     * @return Complex number in root
     */
    public ComplexNumber root(double num) {
        double module = module();
        double varphi = (real != 0) ? Math.acos(real / module) : Math.asin(imagine / module);
        return new ComplexNumber(
                Math.pow(module, 1.0 / num) * Math.cos(varphi / num),
                Math.pow(module, 1.0 / num) * Math.sin(varphi / num)
        );
    }
}
