package ru.antosik.complexnumber;

import ru.antosik.complexnumber.Classes.ComplexNumber;

public class Main {

    public static void main(String[] args) {
        ComplexNumber first = new ComplexNumber(1, 1);
        ComplexNumber second = new ComplexNumber(-1, -1);

        System.out.printf("(%s) + (%s) = %s %n", first, second, first.add(second));
        System.out.printf("(%s) - (%s) = %s %n", first, second, first.substract(second));
        System.out.printf("(%s) * (%s) = %s %n", first, second, first.multiply(second));
        System.out.printf("(%s) / (%s) = %s %n", first, second, first.divide(second));
        System.out.printf("Module of (%s) = %s %n", first, first.module());
        System.out.printf("(%s) in 2nd pow = %s %n", first, first.pow(2));
        System.out.printf("2nd root of (%s)  = %s %n", first, first.root(2));
    }
}
